let collection = [];

/*Required queue functions:
 - show all the items in the queue - print
 - push an element in the queue - enqueue 
 - pop an element in the queue - dequeue
 - show the top element in the queue -front
 - return the size of the queue - size
 - return if there are items in the queue or not - isEmpty
*/


//// Write the queue functions below.


function print(){
	return collection;
}

function enqueue(name){
	collection[collection.length]=name;
	return collection;
}


function dequeue(){
	let newCollection = [];
	
	for(let i = 0; i < collection.length - 1; ++i){
		newCollection[i] = collection[i + 1];
	}

	collection = newCollection

	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length > 0){
		return false;
	}
	else{
		return true;
	}
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
}
