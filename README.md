To start using the built-in tests, run <code>npm install</code> first then issue the <code>npm test</code> at the root directory of this project.

WARNING: Do not change any code inside <code>test.js</code>.


///
Required stack functions:
 - show all the items in the stack
 - push an element in the stack
 - pop an element in the stack
 - show the top element in the stack
 - return the size of the stack
 - return if there are items in the stack or not